import {
  defineConfig,
  extractorSplit,
  presetAttributify,
  presetIcons,
  presetTypography,
  presetUno,
  transformerDirectives,
  transformerVariantGroup,
} from 'unocss'

export default defineConfig({
  theme: {
    colors: {
      'bg': '#4c4f69',
      'bgl': '#5c5f77',
      'accent-p': '#ea76cb',
      'accent-rose': '#dc8a78',
    },
    breakpoints: {
      xs: '380px',
      sm: '640px',
      md: '768px',
      lg: '1024px',
    },
  },
  presets: [
    presetUno(),
    presetAttributify(),
    presetIcons({
      scale: 1.2,
      warn: true,
    }),
    presetTypography(),
  ],
  extractors: [
    extractorSplit,
  ],
  transformers: [
    transformerDirectives(),
    transformerVariantGroup(),
  ],
  shortcuts: {
    btn: 'rounded-md p-1 bg-gray-200 hover:bg-gray-300 tracking-wide text-gray-500',
    unimportant: 'text-gray-400 font-italic tracking-wide',
  },
})
