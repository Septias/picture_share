import '~/index.sass'
import 'virtual:uno.css'
import '@unocss/reset/tailwind.css'
import { type Component, createEffect, createMemo } from 'solid-js'
import { For, render } from 'solid-js/web'
import { createStore } from 'solid-js/store'

interface image {
  preview: string
  identifier: string
  name: string
}
interface fullImage {
  data: string
  preview: string
  identifier: string
  name: string
}

enum MessageType {
  NewImage,
  DownloadResponse,
  DownloadRequest,
}

interface NewImageMsg {
  type: MessageType.NewImage
  name: string
  preview: string
  identifier: string
}

interface DownloadResponse {
  type: MessageType.DownloadResponse
  image: fullImage
  identifier: string
  salt: string
}

interface DownloadRequest {
  type: MessageType.DownloadRequest
  identifier: string
  salt: string
}

function isNewImageMsg(msg: any): msg is NewImageMsg {
  return msg.type === MessageType.NewImage
}

function isDownloadResponse(msg: any): msg is DownloadResponse {
  return msg.type === MessageType.DownloadResponse
}

function isDownloadRequest(msg: any): msg is DownloadRequest {
  return msg.type === MessageType.DownloadRequest
}

function isFullImage(msg: any): msg is fullImage {
  return 'data' in msg
}

const Store: Component = () => {
  const [pictures, setPictures] = createStore({} as Record<string, (image | fullImage)>)
  const cache = localStorage.getItem('pictures')
  if (cache) {
    setPictures(JSON.parse(cache))
  }
  createEffect(() => localStorage.setItem('pictures', JSON.stringify(pictures)))
  const image_list = createMemo(() => Object.values(pictures))
  const requested_images: Set<string> = new Set()

  const onUpload = () => {
    console.log('Uploading new images')
    window.webxdc.importFiles({ mimeTypes: ['png', 'jpg'], multiple: true }).then((files) => {
      for (const image of files) {
        const reader = new FileReader()
        reader.readAsDataURL(image)
        reader.onload = () => {
          const data = reader.result as string
          const identifier = Math.random().toString(36).substring(7)
          const new_image: fullImage = { data, preview: data, identifier, name: image.name }
          setPictures(identifier, new_image)
          window.webxdc.sendUpdate({
            payload: { type: MessageType.NewImage, identifier, preview: data } as NewImageMsg,
            info: '.. Added image',
          }, '.. Added image')
        }
      }
    })
  }

  const onDownload = (image: fullImage | image) => {
    if (isFullImage(image)) {
      console.log('Sending image to chat', image.data)
      window.webxdc.sendToChat({ text: '', file: { base64: image.data.substring(22), name: image.name } })
    }
    else {
      const salt = Math.random().toString(36).substring(7)
      requested_images.add(image.identifier)
      window.webxdc.sendUpdate({
        payload: { type: MessageType.DownloadRequest, identifier: image.identifier, salt } as DownloadRequest,
        info: '.. Requested image',
      }, '.. Requested image')
    }
  }

  window.webxdc.setUpdateListener(({ payload }) => {
    console.log('received update')

    if (isNewImageMsg(payload)) {
      console.log('Rceived new image')
      setPictures(payload.identifier, payload)
    }
    else if (isDownloadResponse(payload) && requested_images.has(payload.identifier)) {
      console.log('Received image download', payload)
      requested_images.delete(payload.identifier)
      setPictures(payload.identifier, payload.image)
    }
    else if (isDownloadRequest(payload)) {
      if (isFullImage(pictures[payload.identifier])) {
        console.log('Sending image download')
        window.webxdc.sendUpdate({
          payload: { type: MessageType.DownloadResponse, image: pictures[payload.identifier], identifier: payload.identifier, salt: payload.salt } as DownloadResponse,
          info: '.. Sending image',
        }, '.. Sending image')
      }
    }
    else {
      console.log('Received unknown message:', payload)
    }
  })

  return (
    <div class="c-grid">
        <div class="min-width p-2">
          <div class="my-4 flex items-center justify-between gap-2">
            <h1 class="text-accent-rose text-center text-3xl font-bold leading-none"> Webxdc Picture Sharing </h1>
            <button class="border-accent-rose text-accent-rose border rounded-xl px-3" onclick={onUpload}> Upload </button>
          </div>
          <div class="picture-grid justify-center">
            <For each={image_list()}>{(image: (fullImage | image)) => {
              return (<button class="bg-bgl aspect-1 rounded" classList={{ 'border border-red': !isFullImage(image) }} onclick={() => onDownload(image)}>
                <img class="rounded" src={image.preview}></img>
              </button>)
            }}</For>
          </div>
        </div>
    </div>
  )
}

const root = document.getElementById('root')
render(() => <Store />, root!)
