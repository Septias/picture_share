#!/bin/sh

set -e
cd dist

echo "Building galery.xdc"
zip -9 --recurse-paths "galery.xdc" *
cp galery.xdc ../
